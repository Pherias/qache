const database = require('./database');

class Store {
    constructor(name) {
        this.name = name;
        this.active = false;
    }

    async Initialize() {
        try {
            database.run(`
                CREATE TABLE IF NOT EXISTS ${this.name} (
                    key TEXT PRIMARY KEY,
                    value TEXT NOT NULL
                )
            `)
            this.active = true;
        } catch (error) {
            this.active = false;
        }

    }

    async Has(key) {
        if (!this.active) return false;

        return new Promise((resolve) => {
            database.get(`
                select 1
                from ${this.name}
                where key = ?
                limit 1;
            `, [key],(error, result ) => {
                if (error) {
                    this.TurnOff();
                    resolve(false)
                }
                else resolve(result instanceof Object)
            });
        })
    }

    async Get(key) {
        if (!this.active) return null;

        return new Promise((resolve, reject) => {
            try {
                database.get(`
                    select *
                    from ${this.name}
                    where key = ?
                    limit 1;
                `, [key], (error, result) => {
                    if (error) {
                        this.TurnOff();
                        resolve(null);
                    } else {
                        if (!result) return resolve(null);
                        console.log(`using cache from ${this.name}`);
                        try {
                            const value = JSON.parse(result.value);
                            resolve(value);
                        } catch (e) {
                            resolve (result.value);
                        }
                    }
                });
            } catch (error) {
                this.TurnOff();
                resolve(null);
            }
        })
    }

    async Add(key, value) {
        if (!this.active) return;

        return new Promise((resolve, reject) => {
            try {
                if (value instanceof Object || value instanceof Array)
                    value = JSON.stringify(value);
    
                const query = `INSERT INTO ${this.name} VALUES (?, ?)`;

                statement.run(query, [key, value], (error) => {
                    if (error) throw new Error(error);
                    console.log('insert');
                    resolve();
                });
            } catch (_) {
                resolve();
                this.TurnOff();
            }
        })
    }

    async Remove(key) {
        if (!this.active) return;

        return new Promise((resolve) => {
            try {
                database.run(`
                    delete from ${this.name}
                    where key = ?
                `, [key], (error) => {
                    if (error) this.TurnOff();
                    resolve();
                })
            } catch (_) {
                this.TurnOff();
                resolve();
            }
        })
    }

    TurnOff() {
        this.active = false;
    }
}

module.exports = Store;