const Store = require("./Store");

class Qache {
    constructor() {
        this.stores = {};

        console.log(`new qache instance created`);
    }

    async Initialize() {
        const promises = Object.keys(this.stores).map(name => this.stores[name].Initialize());

        await Promise.all(promises);

        console.log(`qache stores have been initialized`);
    }

    Configure(name) {
        name = name.replace(/[^a-zA-Z ]/g, '');

        const store = new Store(name);
        this.stores[name] = store;

        return store;
    }


}

module.exports = Qache;